import { Action } from '@ngrx/store';

export enum AppComponentActionTypes {
  LoadAppComponents = '[AppComponent] Load AppComponents',
  ChangeBackground = '[AppComponent] Change background',
  AsyncStart = '[AppComponent] Async start',
  AsyncSuccess = '[AppComponent] Async success',
  AsyncFail = '[AppComponent] Async fail'
}

export class LoadAppComponents implements Action {
  readonly type = AppComponentActionTypes.LoadAppComponents;
}

export class ChangeBackground implements Action {
  readonly type = AppComponentActionTypes.ChangeBackground;
}
export class AsyncStart implements Action {
  readonly type = AppComponentActionTypes.AsyncStart;
  constructor(public num: number) {}
}
export class AsyncSuccess implements Action {
  readonly type = AppComponentActionTypes.AsyncSuccess;
  constructor(public num: number) {}
}
export class AsyncFail implements Action {
  readonly type = AppComponentActionTypes.AsyncFail;
  constructor(public message: string) {}
}

export type AppComponentActions =
  | LoadAppComponents
  | ChangeBackground
  | AsyncStart
  | AsyncSuccess
  | AsyncFail;
