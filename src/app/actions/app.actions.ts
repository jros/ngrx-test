import { Action } from '@ngrx/store';

export enum AppActionTypes {
  InitApp = '[App] Init'
}

export class InitApp implements Action {
  readonly type = AppActionTypes.InitApp;
}

export type AppActions = InitApp;
