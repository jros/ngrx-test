import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { AppActionTypes } from '../actions/app.actions';
import {
  AppComponentActionTypes,
  AsyncStart,
  AsyncSuccess,
  AsyncFail
} from '../actions/app-component.actions';
import { tap, map, exhaustMap, catchError, delay } from 'rxjs/operators';
import { Observable, Observer, of } from 'rxjs';

@Injectable()
export class AppComponentEffects {
  @Effect()
  asyncStart$ = this.actions$.pipe(
    ofType(AppComponentActionTypes.AsyncStart),
    map((action: AsyncStart) => {
      return action.num;
    }),
    exhaustMap((num: number) => {
      return of(num + 30)
        .pipe(delay(1000))
        .pipe(
          map((newNum: number) => new AsyncSuccess(newNum)),
          catchError(error => of(new AsyncFail(JSON.stringify(error))))
        );
    })
  );

  constructor(private actions$: Actions) {}
}
