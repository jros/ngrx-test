import { TestBed, inject } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { AppComponentEffects } from './app-component.effects';

describe('AppComponentService', () => {
  let actions$: Observable<any>;
  let effects: AppComponentEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AppComponentEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get(AppComponentEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
