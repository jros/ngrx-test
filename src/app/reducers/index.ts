import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as fromApp from './app.reducer';
import * as fromAppComponent from './app-component.reducer';

export interface State {
  app: fromApp.State;
  appComponent: fromAppComponent.State;
}

export const reducers: ActionReducerMap<State> = {
  app: fromApp.reducer,
  appComponent: fromAppComponent.reducer
};

export const metaReducers: MetaReducer<State>[] = !environment.production
  ? []
  : [];

export const getAppComponentState = (state: State) => state.appComponent;
