import { Action } from '@ngrx/store';
import { AppActionTypes } from '../actions/app.actions';

export interface State {}

export const initialState: State = {};

export function reducer(state = initialState, action: Action): State {
  switch (action.type) {
    case AppActionTypes.InitApp:
      return state;
    default:
      return state;
  }
}
