import { Action, createSelector } from '@ngrx/store';
import {
  AppComponentActionTypes,
  AsyncStart,
  AsyncSuccess,
  AsyncFail
} from '../actions/app-component.actions';

export interface State {
  bgColor: string;
  message: string;
}

export const initialState: State = {
  bgColor: '#ffffff',
  message: 'Not launched!'
};

export function reducer(state = initialState, action: Action): State {
  switch (action.type) {
    case AppComponentActionTypes.ChangeBackground:
      return Object.assign({}, state, {
        bgColor: state.bgColor === '#ffffff' ? '#7f7f7f' : '#ffffff'
      });
    case AppComponentActionTypes.AsyncStart:
      return Object.assign({}, state, {
        message: 'Starting with ' + (action as AsyncStart).num
      });
    case AppComponentActionTypes.AsyncSuccess:
      return Object.assign({}, state, {
        message: 'Success with ' + (action as AsyncSuccess).num
      });
    case AppComponentActionTypes.AsyncFail:
      return Object.assign({}, state, {
        message: 'Fail with message: ' + (action as AsyncFail).message
      });
    default:
      return state;
  }
}
