import { Component } from '@angular/core';
import { Store, select } from '@ngrx/store';
import * as fromRoot from './reducers';
import { ChangeBackground, AsyncStart } from './actions/app-component.actions';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: []
})
export class AppComponent {
  title = 'app';
  bgColor$: Observable<string>;
  message$: Observable<string>;

  constructor(private store: Store<fromRoot.State>) {
    this.bgColor$ = store.pipe(
      select(fromRoot.getAppComponentState),
      select(state => state.bgColor)
    );
    this.message$ = store.pipe(
      select(fromRoot.getAppComponentState),
      select(state => state.message)
    );
    // store.subscribe(state => {
    //   this.bgColor = fromRoot.getAppComponentState
    // });
  }

  changeBackground() {
    this.store.dispatch(new ChangeBackground());
  }

  asyncStart() {
    this.store.dispatch(new AsyncStart(4));
  }
}
